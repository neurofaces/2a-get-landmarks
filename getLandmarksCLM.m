function landmarks = getLandmarksCLM(test_image, image_name, clmParams, pdm, patches, models_folder)

clmParams.multi_modal_types  = patches(1).multi_modal_types;
verbose = false;

% % Detect faces
disp(['CLM: Detecting Face in ' ,image_name]);

% % Zhu and Ramanan and Yu et al. are slower, but also more accurate 
% % and can be used when vision toolbox is unavailable
[bbox, landmarks] = detect_faces(test_image, {'yu', 'zhu'}, models_folder);

if(size(test_image,3) == 3)
    image = rgb2gray(test_image);
end              

if(verbose)
    f = figure;    
    if(max(image(:)) > 1)
        imshow(double(test_image)/255, 'Border', 'tight');
    else
        imshow(double(test_image), 'Border', 'tight');
    end
    axis equal;
    hold on;
end

% Convert from the initial detected shape to CLM model parameters,
% if shape is available

disp(['CLM: Fitting ' image_name]);

if(~isempty(landmarks))
    inds = [1:60,62:64,66:68];
    M = pdm.M([inds, inds+68, inds+68*2]);
    E = pdm.E;
    V = pdm.V([inds, inds+68, inds+68*2],:);
    [ a, R, T, ~, params, err, shapeOrtho] = fit_PDM_ortho_proj_to_2D(M, E, V, landmarks);
    g_param = [a; Rot2Euler(R)'; T];
    l_param = params;

    % Use the initial global and local params for clm fitting in the image
    [landmarks,~,~,lhood,lmark_lhood,view_used] = Fitting_from_bb(image, [], bbox, pdm, patches, clmParams, 'gparam', g_param, 'lparam', l_param);
else
    [landmarks,~,~,lhood,lmark_lhood,view_used] = Fitting_from_bb(image, [], bbox, pdm, patches, clmParams);
end

% shape correction for matlab format
landmarks = landmarks + 1;

if(verbose)

    % valid points to draw (not to draw self-occluded ones)
    v_points = logical(patches(1).visibilities(view_used,:));

    try

    plot(landmarks(v_points,1), landmarks(v_points',2),'.r','MarkerSize',20);
    plot(landmarks(v_points,1), landmarks(v_points',2),'.b','MarkerSize',10);

    catch warn

    end
end

hold off;