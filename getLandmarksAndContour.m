% ####################################################################### %
% #####################  GET LANDMARKS AND CONTOUR ###################### %
% ####################################################################### %
%                                                                         %
% First: use Chehra algorithm for facial features detection               %
% Second: use CLM-framework for facial contour detection                  %
%                                                                         %
% ####################################################################### %
function landmarks = getLandmarksAndContour()

verbose = true;
verbose_chehra = false;

% % Display status
disp('Loading models...');

% % Images Test Path
% image_path='test-images-cfd/fail-tests/'; % possible-fails
image_path=fullfile('C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\', 'cfd-boy-neutral-allraces\');
% image_path = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test\';
% image_path = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized-175-IOD\';
% results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized-175-IOD\landmarks\';
results_folder = [image_path '\landmarks\'];
if(~exist(results_folder, 'dir')), mkdir(results_folder); end

% extension = '.bmp';
extension = '.jpg';

% % ---------------------------------------------------------------------->

% % Algorithm folder
alg_folder = '../_common_functions/';

% % Add Chehra folder to the path
chehra_folder = fullfile(alg_folder, 'Chehra_v0.1_MatlabFit');
addpath(genpath(chehra_folder));
% % ---------------------------------------------------------------------->

% % Add CLM-framework folders to the path
CLM_framework_folder = fullfile(alg_folder, 'CLM-framework');
addpath(fullfile(CLM_framework_folder, 'PDM_helpers'));
addpath(genpath(fullfile(CLM_framework_folder, 'fitting')));
addpath(fullfile(CLM_framework_folder, 'models'));
addpath(fullfile(CLM_framework_folder, 'models', 'pdm'));
addpath(genpath(fullfile(CLM_framework_folder, 'face_detection')));
addpath(fullfile(CLM_framework_folder, 'CCNF'));
% % ---------------------------------------------------------------------->

% ####################################################################### %
% ################  Use CHEHRA to detect facial features  ############### %
% #############  Use CLM-landmarks to detect facial contour  ############ %
% ####################################################################### %

% % CHEHRA Landmarks labels
lm.chehra.LeftEyebrow = 1:5;
lm.chehra.RightEyebrow = 6:10;
lm.chehra.LeftEye = 20:25;
lm.chehra.RightEye = 26:30;
lm.chehra.Nose = [11, 15, 19]; % 11:14;
% lm.chehra.Nostrils = 15:19;
lm.chehra.OutterMouth = 32:43;
% lm.chehra.InnerMouth = 44:49;
% % ---------------------------------------------------------------------->

% % CLM-framework Landmarks labels
lm.CLM.LeftEyebrow = 1:5;
lm.CLM.RightEyebrow = 6:10;
lm.CLM.LeftEye = 20:25;
lm.CLM.RightEye = 26:30;
lm.CLM.Nose = [11, 15, 19]; % 11:14;
% lm.CLM.Nostrils = 15:19;
lm.CLM.OutterMouth = 32:43;
% lm.CLM.InnerMouth = 44:49;
lm.CLM.FaceContour = 1:17;
% % ---------------------------------------------------------------------->

% % Load Chehra Model
fitting_model=fullfile(chehra_folder, 'models/Chehra_f1.0.mat');
load(fitting_model);
% % ---------------------------------------------------------------------->

% % Load CLM-framework Model
% % Loading the patch experts
[clmParams, pdm] = Load_CLM_params_wild();
% % An accurate CCNF (or CLNF) model
[patches] = Load_Patch_Experts(fullfile(CLM_framework_folder, '/models/general/'), 'ccnf_patches_*_general.mat', [], [], clmParams);
% % A simpler (but less accurate SVR)
% [patches] = Load_Patch_Experts(fullfile(CLM_framework_folder, '/models/general/'), 'svr_patches_*_general.mat', [], [], clmParams);
% % ---------------------------------------------------------------------->

% % Load images in test path
img_list=dir([image_path,'*' extension]);
n_images = size(img_list,1);

% % Create structure to store landmarks and bboxes
nland = (17+49)*2;
all_images_landmarks = nan(n_images, nland);
bboxes = nan(n_images, 4);

% % Perform Fitting
for ii=1:n_images
    
    close all;
    
    % % Disp status
    disp(['Processing image ', num2str(ii), '/', num2str(n_images), '.']);

    % % Load Image
    test_image = im2double(imread([image_path,img_list(ii).name]));
    figure1 = figure;
    imshow(test_image);
    hold on;
    
    % % Resize image to speed up the process
    size_h = size(test_image, 2);
    size_resized_h = 512;
    if(size_h > size_resized_h)
        ratio = size_resized_h / size_h;
        test_image_res = imresize(test_image, ratio);
    else
        ratio = 1;
        test_image_res = test_image;
    end
    % % Get landmarks and bbox with Chehra algorithm - accurate
    [landmarks_res, bbox_res] = getLandmarksChehra(test_image_res, img_list(ii).name, refShape, RegMat, verbose_chehra, CLM_framework_folder);
    % % If CHEHRA cannot detect face, discard image
    if(landmarks_res == -1), disp('Image skipped. Couldn''t detect any face.'), continue; end
    % % Relocate landmarks in original sized image
    landmarks = landmarks_res / ratio;
    bbox = bbox_res / ratio;
    
    % % Get face contour with CLM-framework - gets contour and landmarks,
    % % but landmarks are less accurate than those obtained by Chehra
    try
        face_contour = getLandmarksCLM(test_image, img_list(ii).name, clmParams, pdm, patches, CLM_framework_folder);
    catch e
        disp(e.getReport());
        fclose(fopen(fullfile(results_folder, strrep(img_list(ii).name, extension, '_couldnt_detect_face_contour.log')), 'w'));
        face_contour = zeros(17, 2);
    end
    
    % % Join facial contour points and fiducials detected
    face_contour = face_contour(1:17,:); % keep only face's contour
    landmarks = [face_contour(1:17,:); landmarks];
    all_images_landmarks(ii,:) = landmarks(:)';
    bboxes(ii, :) = bbox;
    
    % % Plot face contour, landmarks and eye centroids
    hold on;
    plot(landmarks(:,1), landmarks(:,2), 'g*', 'MarkerSize', 3); 
    legend('Initialization', 'Final Fitting');
    set(gcf, 'units', 'normalized', 'outerposition',[0 0 1 1]);
    
    % % Save detected landmarks in faces and its locations
    fp = fopen(fullfile(results_folder, strrep(img_list(ii).name, extension, '.lm')), 'w');
    for nl=1:size(landmarks, 1)
        fprintf(fp,'%5f %5f\n',landmarks(nl, :));
    end
    fclose(fp);
    saveas(figure1, [results_folder, strrep(img_list(ii).name, extension, ['_lm' extension])]);
    % pause;
    
end

save('all_images_landmarks', 'all_images_landmarks', 'bboxes');
